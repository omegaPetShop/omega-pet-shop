const express = require('express');
const router = express.Router();

const modeloCarrito = require('../models/model_carrito');

router.get('/listar', (req, res)=>{
    modeloCarrito.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs);

        }
        else
        {
            res.send(err);

        }

    })
})

router.get('/cargar/:id', (req, res)=>{
    modeloCarrito.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs);

        }
        else
        {
            res.send(err);

        }

    })
})

router.post('/agregar', (req, res)=>{

    const nuevoCarrito = new modeloCarrito({
        id : req.body.id,
        cliente : req.body.cliente,
        fecha : req.body.fecha,
        pagado : req.body.pagado
    });

    nuevoCarrito.save(function(err)
    {
        if(!err)
        {
            res.send("Su Carrito fue agregado con exito");

        }
        else
        {
            res.send(err.stack);

        }

    });
})

router.post('/editar/:id', (req, res)=>{
    modeloCarrito.findOneAndUpdate({id:req.params.id}, {
        cliente : req.body.cliente,
        fecha : req.body.fecha,
        pagado : req.body.pagado
    }, (err)=>
    {
        if(!err)
        {
            res.send("Su Carrito fue editado con exito");

        }
        else
        {
            res.send(err.stack);

        }
     })
});

router.delete('/eliminar/:id', (req, res)=>{
    modeloCarrito.findOneAndDelete({id:req.params.id}, (err)=>
    {
        if(!err)
        {
            res.send("Su Carrito fue eliminado con exito");

        }
        else
        {
            res.send(err.stack);

        }
     })
});


module.exports = router;