import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

/*
import Checkbox from '@material-ui/core/Checkbox';
export default function Checkboxes()
{
    const [checked, setChecked] = React.useState(true);
    const handleChange = (event) => {
    setChecked(event.target.checked);
};
<Checkbox checked={checked} onChange={handleChange} inputProps={{ 'aria-label': 'primary checkbox' }}/>
*/
function EnviosEditar()
{
    const parametros = useParams()
    const[id, setId] = useState('')
    const[nombre, setNombre] = useState('')
    const[direccion, setDireccion] = useState('')
    const[pais, setPais] = useState('')
    const[departamento, setDepartamento] = useState('')
    const[ciudad, setCiudad] = useState('')
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/pedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/info/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/pedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataEnvios = res.data[0]
        setId(dataEnvios.id)
        setNombre(dataEnvios.nombre)
        setDireccion(dataEnvios.direccion)
        setPais(dataEnvios.pais)
        setDepartamento(dataEnvios.departamento)
        setCiudad(dataEnvios.ciudad)
        })
    }, [])

    function enviosActualizar()
    {
        const envioactualizar = {
            id: parametros.id,
            nombre: nombre,
            direccion: direccion,
            pais: pais,
            departamento: departamento,
            ciudad: ciudad 
        }

        console.log(envioactualizar)

        axios.post(`/api/info/editar/${parametros.id}`,envioactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/envioslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function enviosRegresar()
    {
        //window.location.href="/";
        navigate('/envioslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Editar Envio</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="id" className="form-label">id</label>
                        <input type="text" className="form-control" id="id" value={id} onChange={(e) => {setId(e.target.value)}}></input>
                        </div>                    
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="direccion" className="form-label">Dirección</label>
                        <input type="text" className="form-control" id="direccion" value={direccion} onChange={(e) => {setDireccion(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="pais" className="form-label">Pais</label>
                        <input type="text" className="form-control" id="pais" value={pais} onChange={(e) => {setPais(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="departamento" className="form-label">Departamento</label>
                        <input type="text" className="form-control" id="departamento" value={departamento} onChange={(e) => {setDepartamento(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="ciudad" className="form-label">Ciudad</label>
                        <input type="text" className="form-control" id="ciudad" value={ciudad} onChange={(e) => {setCiudad(e.target.value)}}></input>
                    </div>         
                    <div className="mb-12">
                        <button type="button" onClick={enviosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={enviosActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

//<div className="mb-3 form-check form-switch">
//<input className="form-check-input" type="checkbox" role="switch" id="activo"></input>
//</div>

export default EnviosEditar;