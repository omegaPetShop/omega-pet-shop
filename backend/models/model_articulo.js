const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const esquemaArticulo = new miesquema({
    carrito : String,
    producto : String,
    cantidad : String
})

const modeloArticulo = mongoose.model("articulos",esquemaArticulo );

module.exports = modeloArticulo;