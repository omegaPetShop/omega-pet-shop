import axios from 'axios';
import uniquid from 'uniquid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function ProductosAgregar()
{
    const[id, setId] = useState('')
    const[id_categoria, setId_categoria] = useState('')
    const[nombre, setNombre] = useState('')
    const[descripcion, setDescripcion] = useState('')
    const[precio, setPrecio] = useState('')
    const[activo, setActivo] = useState('')
    const navigate = useNavigate()
    
    function productosInsertar()
    {

        const productoinsertar = {
            id: id,
            id_categoria: id_categoria,
            nombre: nombre,
            descripcion: descripcion,
            precio: precio,
            activo: activo
        }

        console.log(productoinsertar)

        axios.post(`/api/producto/agregar`,productoinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/productoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function productosRegresar()
    {
        window.location.href="/productoslistar";
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Producto</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="id" className="form-label">id</label>
                        <input type="text" className="form-control" id="id" value={id} onChange={(e) => {setId(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="id_categoria" className="form-label">Id Categoria</label>
                        <input type="text" className="form-control" id="id_categoria" value={id_categoria} onChange={(e) => {setId_categoria(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="descripcion" className="form-label">Descripción</label>
                        <input type="text" className="form-control" id="descripcion" value={descripcion} onChange={(e) => {setDescripcion(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="precio" className="form-label">Precio</label>
                        <input type="text" className="form-control" id="precio" value={precio} onChange={(e) => {setPrecio(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>         
                    <div className="mb-12">
                        <button type="button" onClick={productosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={productosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default ProductosAgregar;