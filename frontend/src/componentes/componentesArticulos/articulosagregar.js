import axios from 'axios';
import uniquid from 'uniquid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function ArticulosAgregar()
{
    const[carrito, setCarrito] = useState('')
    const[producto, setProducto] = useState('')
    const[cantidad, setCantidad] = useState('') 
    const navigate = useNavigate()
    
    function articulosInsertar()
    {

        const articuloinsertar = {
            carrito: carrito,
            producto: producto,
            cantidad: cantidad
        }

        console.log(articuloinsertar)

        axios.post(`/api/articulo/agregar`,articuloinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/articuloslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function articulosRegresar()
    {
        window.location.href="/articuloslistar";
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Articulo</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="carrito" className="form-label">Carrito</label>
                        <input type="text" className="form-control" id="carrito" value={carrito} onChange={(e) => {setCarrito(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="producto" className="form-label">Producto</label>
                        <input type="text" className="form-control" id="producto" value={producto} onChange={(e) => {setProducto(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="cantidad" className="form-label">Cantidad</label>
                        <input type="text" className="form-control" id="cantidad" value={cantidad} onChange={(e) => {setCantidad(e.target.value)}}></input>
                    </div>               
                    <div className="mb-12">
                        <button type="button" onClick={articulosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={articulosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default ArticulosAgregar;