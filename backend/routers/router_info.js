const express = require('express');
const router = express.Router();

const controladorInfo = require('../controllers/controller_info');

router.get("/listar", controladorInfo);
router.get('/cargar/:id', controladorInfo);
router.post('/agregar', controladorInfo);
router.post('/editar/:id', controladorInfo);
router.delete('/eliminar/:id', controladorInfo);



module.exports = router