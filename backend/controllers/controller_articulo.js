const express = require('express');
const router = express.Router();

const modeloArticulo = require('../models/model_articulo');

router.get('/listar', (req, res)=>{
    modeloArticulo.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs);

        }
        else
        {
            res.send(err);

        }

    })
})

router.get('/cargar/:carrito', (req, res)=>{
    modeloArticulo.find({carrito:req.params.carrito}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs);

        }
        else
        {
            res.send(err);

        }

    })
})

router.post('/agregar', (req, res)=>{

    const nuevoArticulo = new modeloArticulo({
        carrito : req.body.carrito,
        producto : req.body.producto,
        cantidad : req.body.cantidad
    });

    nuevoArticulo.save(function(err)
    {
        if(!err)
        {
            res.send("Su Articulo fue agregado con exito");

        }
        else
        {
            res.send(err.stack);

        }

    });
})

router.post('/editar/:carrito', (req, res)=>{
    modeloArticulo.findOneAndUpdate({carrito:req.params.carrito}, {
        producto : req.body.producto,
        cantidad : req.body.cantidad
    }, (err)=>
    {
        if(!err)
        {
            res.send("Su Articulo fue editado con exito");

        }
        else
        {
            res.send(err.stack);

        }
     })
});

router.delete('/eliminar/:carrito', (req, res)=>{
    modeloArticulo.findOneAndDelete({carrito:req.params.carrito}, (err)=>
    {
        if(!err)
        {
            res.send("Su Articulo fue eliminado con exito");

        }
        else
        {
            res.send(err.stack);

        }
     })
});


module.exports = router;