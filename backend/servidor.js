const express = require('express');
const app = express();


const miconexion = require('./conexion')

const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));


const rutas = require('./routers/routers')
app.use("/api", rutas)


app.get('/', (req, res) =>{
    res.send('La pagina si esta funcionando ;)')
})

app.listen(5000, function()
{
    console.log("Si estoy funcionando en el puerto 5000 - http://localhost:5000")
})