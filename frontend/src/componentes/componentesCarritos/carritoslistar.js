import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import CarritosBorrar from './carritoseliminar';


function CarritosListar()
{
    const[dataCarritos, setdataCarritos] = useState([])

    useEffect(()=>{
        axios.get('api/carrito/listar').then(res => {
        console.log(res.data)
        setdataCarritos(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])

return (
    <div className="container mt-5">
    <h4>Lista de Carritos</h4>
    <div className="row">
        <div className="col-md-12">
            <table className="table table-dark">
                <thead>
                    <tr key={0}>
                        <td colSpan={6} align="right"><Link to={`/carritosagregar`}><li className='btn btn-success'>Agregar Carrito</li></Link></td>
                    </tr>
                    <tr key={0}>
                        <td align="center">Id</td>
                        <td>Cliente</td>
                        <td>Fecha</td>
                        <td>Pagado</td>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>
                </thead> 
                <tbody className='dialog'>
                {
                    dataCarritos.map(micarrito => (
                    <tr key={micarrito.id}>
                        <td align="center">{micarrito.id}</td>
                        <td>{micarrito.cliente}</td>
                        <td>{micarrito.fecha}</td>
                        <td align="right">{micarrito.pagado ? 'Pagado' : 'No Pagado'}</td>
                        <td align="center"><Link to={`/carritoseditar/${micarrito.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                        <td align="center"><li className='btn btn-danger' onClick={()=>{CarritosBorrar(micarrito.id)}}>Borrar</li></td>
                    </tr>
                ))
                }
                </tbody>
            </table>
        </div>
    </div>
    </div>
)

}


export default CarritosListar;