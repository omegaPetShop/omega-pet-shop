const express = require('express');
const router = express.Router();

const modeloInfo = require('../models/model_info');

router.get('/listar', (req, res)=>{
    modeloInfo.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs);

        }
        else
        {
            res.send(err);

        }

    })
})

router.get('/cargar/:id', (req, res)=>{
    modeloInfo.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs);

        }
        else
        {
            res.send(err);

        }

    })
})

router.post('/agregar', (req, res)=>{

    const nuevaInfo = new modeloInfo({
        id : req.body.id,
        nombre : req.body.nombre,
        direccion : req.body.direccion,
        pais : req.body.pais,
        departamento : req.body.departamento,
        ciudad: req.body.ciudad
    });

    nuevaInfo.save(function(err)
    {
        if(!err)
        {
            res.send("Su información fue agregada con exito");

        }
        else
        {
            res.send(err.stack);

        }

    });
})

router.post('/editar/:id', (req, res)=>{
    modeloInfo.findOneAndUpdate({id:req.params.id}, {
        nombre : req.body.nombre,
        direccion : req.body.direccion,
        pais : req.body.pais,
        departamento : req.body.departamento,
        ciudad: req.body.ciudad
    }, (err)=>
    {
        if(!err)
        {
            res.send("Su Información fue editada con exito");

        }
        else
        {
            res.send(err.stack);

        }
     })
});

router.delete('/eliminar/:id', (req, res)=>{
    modeloInfo.findOneAndDelete({id:req.params.id}, (err)=>
    {
        if(!err)
        {
            res.send("Su Información fue eliminada con exito");

        }
        else
        {
            res.send(err.stack);

        }
     })
});





module.exports = router;