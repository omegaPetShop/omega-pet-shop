const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/omegapetshop');
const miconexion = mongoose.connection;

miconexion.on('connected', () => {
    console.log("La base de datos esta conectada")
})

miconexion.on('error', () => {
    console.log("La base de datos NO esta conectada")
})

module.exports = mongoose;