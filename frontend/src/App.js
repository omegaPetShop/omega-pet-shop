import logo from './logo.svg';
import './App.css';
import perrito from './perrito.jpg'
import ArticulosListar from './componentes/componentesArticulos/articuloslistar';
import ArticulosBorrar from './componentes/componentesArticulos/articuloseliminar';
import ArticulosEditar from './componentes/componentesArticulos/articuloseditar';
import ArticulosAgregar from './componentes/componentesArticulos/articulosagregar';
import ProductosListar from './componentes/componentesProductos/productoslistar';
import ProductosEditar from './componentes/componentesProductos/productoseditar';
import ProductosAgregar from './componentes/componentesProductos/productosagregar';
import PaginaPrincipal from './componentes/paginaprincipal';
import CarritosListar from './componentes/componentesCarritos/carritoslistar';
import CarritosAgregar from './componentes/componentesCarritos/carritosagregar';
import CarritosEditar from './componentes/componentesCarritos/carritoseditar';
import EnviosListar from './componentes/componentesEnvios/envioslistar';
import EnviosAgregar from './componentes/componentesEnvios/enviosagregar';
import EnviosEditar from './componentes/componentesEnvios/envioseditar';


import {BrowserRouter, Routes, Route} from 'react-router-dom';

function App() {
  return (
    <div className="App" style={{backgroundImage: `url(${perrito})`, width: "1000"}}>
      <nav className="navbar navbar-expand-lg bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="/paginaprincipal">OmegaPetShop</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="/articuloslistar">Articulos</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/productoslistar">Productos</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/envioslistar">Envios</a>
              </li>
              <li>
                <a className="nav-link" href="/carritoslistar">Carrito de compras</a>
              </li>
            </ul>
          </div>
        </div>
      </nav> 
      <BrowserRouter>
      <Routes>
        <Route path='/paginaprincipal' element={<PaginaPrincipal/>} exact></Route>
        <Route path='/articuloslistar' element={<ArticulosListar/>} exact></Route>
        <Route path='/productoslistar' element={<ProductosListar/>} exact></Route>
        <Route path='/carritoslistar' element={<CarritosListar/>} exact></Route>
        <Route path='/envioslistar' element={<EnviosListar/>} exact></Route>
        <Route path='/articulosagregar' element={<ArticulosAgregar/>} exact></Route>
        <Route path='/productosagregar' element={<ProductosAgregar/>} exact></Route>
        <Route path='/carritosagregar' element={<CarritosAgregar/>} exact></Route>
        <Route path='/enviosagregar' element={<EnviosAgregar/>} exact></Route>
        <Route path='/articuloseditar/:carrito' element={<ArticulosEditar/>} exact></Route>
        <Route path='/productoseditar/:id' element={<ProductosEditar/>} exact></Route>
        <Route path='/carritoseditar/:id' element={<CarritosEditar/>} exact></Route>
        <Route path='/envioseditar/:id' element={<EnviosEditar/>} exact></Route>
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
