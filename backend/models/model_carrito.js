const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const esquemaCarrito = new miesquema({
    id : String,
    cliente : String,
    fecha : String,
    pagado : Boolean
})

const modeloCarrito = mongoose.model("carritocompras",esquemaCarrito);

module.exports = modeloCarrito;