const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const esquemaInfo = new miesquema({
    id : String,
    nombre : String,
    direccion : String,
    pais : String,
    departamento : String,
    ciudad: String
})

const modeloInfo = mongoose.model("infoenvios",esquemaInfo);

module.exports = modeloInfo;