const express = require('express');
const router = express.Router();

const controladorArticulo = require('../controllers/controller_articulo');

router.get("/listar", controladorArticulo);
router.get('/cargar/:id', controladorArticulo);
router.post('/agregar', controladorArticulo);
router.post('/editar/:carrito', controladorArticulo);
router.delete('/eliminar/:carrito', controladorArticulo)



module.exports = router