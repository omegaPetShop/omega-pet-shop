const express = require('express');
const router = express.Router();

const rutaArticulo = require('./router_articulo');
router.use('/articulo', rutaArticulo);

const rutaProducto = require('./router_producto');
router.use('/producto', rutaProducto);

const rutaCarrito = require('./router_carrito');
router.use('/carrito', rutaCarrito);

const rutaInfo = require('./router_info');
router.use('/info', rutaInfo);

module.exports = router