import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

/*
import Checkbox from '@material-ui/core/Checkbox';
export default function Checkboxes()
{
    const [checked, setChecked] = React.useState(true);
    const handleChange = (event) => {
    setChecked(event.target.checked);
};
<Checkbox checked={checked} onChange={handleChange} inputProps={{ 'aria-label': 'primary checkbox' }}/>
*/
function CarritosEditar()
{
    const parametros = useParams()
    const[id, setId] = useState('')
    const[cliente, setCliente] = useState('')
    const[fecha, setFecha] = useState('')
    const[pagado, setPagado] = useState('')
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/pedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/carrito/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/pedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataCarritos = res.data[0]
        setId(dataCarritos.id)
        setCliente(dataCarritos.cliente)
        setFecha(dataCarritos.fecha)
        setPagado(dataCarritos.pagado)
        })
    }, [])

    function carritosActualizar()
    {
        const carritoactualizar = {
            id: parametros.id,
            cliente: cliente,
            fecha: fecha,
            pagado: pagado
        }

        console.log(carritoactualizar)

        axios.post(`/api/carrito/editar/${parametros.id}`,carritoactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/carritoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function carritosRegresar()
    {
        //window.location.href="/";
        navigate('/carritoslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Editar Carrito</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="id" className="form-label">id</label>
                        <input type="text" className="form-control" id="id" value={id} onChange={(e) => {setId(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="cliente" className="form-label">Cliente</label>
                        <input type="text" className="form-control" id="cliente" value={cliente} onChange={(e) => {setCliente(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="fecha" className="form-label">Fecha</label>
                        <input type="text" className="form-control" id="fecha" value={fecha} onChange={(e) => {setFecha(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="pagado" className="form-label">Pagado</label>
                        <input type="text" className="form-control" id="pagado" value={pagado} onChange={(e) => {setPagado(e.target.value)}}></input>
                    </div>        
                    <div className="mb-12">
                        <button type="button" onClick={carritosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={carritosActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

//<div className="mb-3 form-check form-switch">
//<input className="form-check-input" type="checkbox" role="switch" id="activo"></input>
//</div>

export default CarritosEditar;