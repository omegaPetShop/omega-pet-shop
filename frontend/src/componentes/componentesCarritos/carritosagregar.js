import axios from 'axios';
import uniquid from 'uniquid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function CarritosAgregar()
{
    const[id, setId] = useState('')
    const[cliente, setCliente] = useState('')
    const[fecha, setFecha] = useState('')
    const[pagado, setPagado] = useState('')
    const navigate = useNavigate()
    
    function carritosInsertar()
    {

        const carritoinsertar = {
            id: id,
            cliente: cliente,
            fecha: fecha,
            pagado: pagado
        }

        console.log(carritoinsertar)

        axios.post(`/api/carrito/agregar`,carritoinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/carritoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function carritosRegresar()
    {
        window.location.href="/carritoslistar";
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Carrito</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="id" className="form-label">id</label>
                        <input type="text" className="form-control" id="id" value={id} onChange={(e) => {setId(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="cliente" className="form-label">Cliente</label>
                        <input type="text" className="form-control" id="cliente" value={cliente} onChange={(e) => {setCliente(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="fecha" className="form-label">Fecha</label>
                        <input type="text" className="form-control" id="fecha" value={fecha} onChange={(e) => {setFecha(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="pagado" className="form-label">Pagado</label>
                        <input type="text" className="form-control" id="pagado" value={pagado} onChange={(e) => {setPagado(e.target.value)}}></input>
                    </div>         
                    <div className="mb-12">
                        <button type="button" onClick={carritosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={carritosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default CarritosAgregar;