const express = require('express');
const router = express.Router();

const controladorCarrito = require('../controllers/controller_carrito');

router.get("/listar", controladorCarrito);
router.get('/cargar/:id', controladorCarrito);
router.post('/agregar', controladorCarrito);
router.post('/editar/:id', controladorCarrito);
router.delete('/eliminar/:id', controladorCarrito);



module.exports = router