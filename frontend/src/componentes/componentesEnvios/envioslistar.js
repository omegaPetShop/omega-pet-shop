import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import EnviosBorrar from './envioseliminar';


function EnviosListar()
{
    const[dataEnvios, setdataEnvios] = useState([])

    useEffect(()=>{
        axios.get('api/info/listar').then(res => {
        console.log(res.data)
        setdataEnvios(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])

return (
    <div className="container mt-5">
    <h4>Lista de Envios</h4>
    <div className="row">
        <div className="col-md-12">
            <table className="table table-dark">
                <thead>
                    <tr key={0}>
                        <td colSpan={6} align="right"><Link to={`/enviosagregar`}><li className='btn btn-success'>Agregar Envio</li></Link></td>
                    </tr>
                    <tr key={0}>
                        <td align="center">Id</td>
                        <td>Nombre</td>
                        <td>Dirección</td>
                        <td>Pais</td>
                        <td>Departamento</td>
                        <td>Ciudad</td>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>
                </thead> 
                <tbody className='dialog'>
                {
                    dataEnvios.map(mienvio => (
                    <tr key={mienvio.id}>
                        <td align="center">{mienvio.id}</td>
                        <td>{mienvio.nombre}</td>
                        <td>{mienvio.direccion}</td>
                        <td>{mienvio.pais}</td>
                        <td>{mienvio.departamento}</td>
                        <td align="right">{mienvio.ciudad}</td>
                        <td align="center"><Link to={`/envioseditar/${mienvio.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                        <td align="center"><li className='btn btn-danger' onClick={()=>{EnviosBorrar(mienvio.id)}}>Borrar</li></td>
                    </tr>
                ))
                }
                </tbody>
            </table>
        </div>
    </div>
    </div>
)

}


export default EnviosListar;