import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

/*
import Checkbox from '@material-ui/core/Checkbox';
export default function Checkboxes()
{
    const [checked, setChecked] = React.useState(true);
    const handleChange = (event) => {
    setChecked(event.target.checked);
};
<Checkbox checked={checked} onChange={handleChange} inputProps={{ 'aria-label': 'primary checkbox' }}/>
*/
function ArticulosEditar()
{
    const parametros = useParams()
    const[carrito, setCarrito] = useState('')
    const[producto, setProducto] = useState('')
    const[cantidad, setCantidad] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/pedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/articulo/cargar/${parametros.carrito}`).then(res => {
        //axios.post(`/api/pedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataArticulos = res.data[0]
        setCarrito(dataArticulos.carrito)
        setProducto(dataArticulos.producto)
        setCantidad(dataArticulos.cantidad)
        })
    }, [])

    function articulosActualizar()
    {
        const articuloactualizar = {
            carrito: parametros.carrito,
            producto: producto,
            cantidad: cantidad
        }

        console.log(articuloactualizar)

        axios.post(`/api/articulo/editar/${parametros.carrito}`,articuloactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/articuloslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function articulosRegresar()
    {
        //window.location.href="/";
        navigate('/articuloslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Editar Articulo</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="carrito" className="form-label">Carrito</label>
                        <input type="text" className="form-control" id="carrito" value={carrito} onChange={(e) => {setCarrito(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="producto" className="form-label">Producto</label>
                        <input type="text" className="form-control" id="producto" value={producto} onChange={(e) => {setProducto(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="cantidad" className="form-label">Cantidad</label>
                        <input type="text" className="form-control" id="cantidad" value={cantidad} onChange={(e) => {setCantidad(e.target.value)}}></input>
                    </div>               
                    <div className="mb-12">
                        <button type="button" onClick={articulosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={articulosActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

//<div className="mb-3 form-check form-switch">
//<input className="form-check-input" type="checkbox" role="switch" id="activo"></input>
//</div>

export default ArticulosEditar;