import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import ArticulosBorrar from './articuloseliminar';


function ArticulosListar()
{
    const[dataArticulos, setdataArticulos] = useState([])

    useEffect(()=>{
        axios.get('api/articulo/listar').then(res => {
        console.log(res.data)
        setdataArticulos(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])

return (
    <div className="container mt-5">
    <h4>Lista de Articulos</h4>
    <div className="row">
        <div className="col-md-12">
            <table className="table table-dark">
                <thead>
                    <tr key={0}>
                        <td colSpan={6} align="right"><Link to={`/articulosagregar`}><li className='btn btn-success'>Agregar Articulo</li></Link></td>
                    </tr>
                    <tr key={0}>
                        <td align="center">Carrito</td>
                        <td>Producto</td>
                        <td>Cantidad</td>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>
                </thead> 
                <tbody className='dialog'>
                {
                    dataArticulos.map(miarticulo => (
                    <tr key={miarticulo.carrito}>
                        <td align="center">{miarticulo.carrito}</td>
                        <td>{miarticulo.producto}</td>
                        <td align="right">{miarticulo.cantidad}</td>
                        <td align="center"><Link to={`/articuloseditar/${miarticulo.carrito}`}><li className='btn btn-info'>Editar</li></Link></td>
                        <td align="center"><li className='btn btn-danger' onClick={()=>{ArticulosBorrar(miarticulo.carrito)}}>Borrar</li></td>
                    </tr>
                ))
                }
                </tbody>
            </table>
        </div>
    </div>
    </div>
)

}


export default ArticulosListar;