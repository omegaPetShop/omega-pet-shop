const express = require('express');
const router = express.Router();

const modeloProducto = require('../models/model_producto');

router.get('/listar', (req, res)=>{
    modeloProducto.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs);

        }
        else
        {
            res.send(err);

        }

    })
})

router.get('/cargar/:id', (req, res)=>{
    modeloProducto.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs);

        }
        else
        {
            res.send(err);

        }

    })
})

router.post('/agregar', (req, res)=>{

    const nuevoProducto = new modeloProducto({
        id : req.body.id,
        id_categoria : req.body.id_categoria,
        nombre : req.body.nombre,
        descripcion : req.body.descripcion,
        precio : req.body.precio,
        activo: req.body.activo
    });

    nuevoProducto.save(function(err)
    {
        if(!err)
        {
            res.send("Su Producto fue agregado con exito");

        }
        else
        {
            res.send(err.stack);

        }

    });
})

router.post('/editar/:id', (req, res)=>{
    modeloProducto.findOneAndUpdate({id:req.params.id}, {
        id_categoria : req.body.id_categoria,
        nombre : req.body.nombre,
        descripcion : req.body.descripcion,
        precio : req.body.precio,
        activo: req.body.activo
    }, (err)=>
    {
        if(!err)
        {
            res.send("Su Producto fue editado con exito");

        }
        else
        {
            res.send(err.stack);

        }
     })
});

router.delete('/eliminar/:id', (req, res)=>{
    modeloProducto.findOneAndDelete({id:req.params.id}, (err)=>
    {
        if(!err)
        {
            res.send("Su Producto fue eliminado con exito");

        }
        else
        {
            res.send(err.stack);

        }
     })
});

module.exports = router;