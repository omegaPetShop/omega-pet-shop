const express = require('express');
const router = express.Router();

const controladorProducto = require('../controllers/controller_producto');

router.get("/listar", controladorProducto);
router.get('/cargar/:id', controladorProducto);
router.post('/agregar', controladorProducto);
router.post('/editar/:id', controladorProducto);
router.delete('/eliminar/:id', controladorProducto);



module.exports = router