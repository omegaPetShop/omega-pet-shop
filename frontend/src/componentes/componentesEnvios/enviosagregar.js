import axios from 'axios';
import uniquid from 'uniquid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function EnviosAgregar()
{
    const[id, setId] = useState('')
    const[nombre, setNombre] = useState('')
    const[direccion, setDireccion] = useState('')
    const[pais, setPais] = useState('')
    const[departamento, setDepartamento] = useState('')
    const[ciudad, setCiudad] = useState('')
    const navigate = useNavigate()
    
    function enviosInsertar()
    {

        const envioinsertar = {
            id: id,
            nombre: nombre,
            direccion: direccion,
            pais: pais,
            departamento: departamento,
            ciudad: ciudad 
        }

        console.log(envioinsertar)

        axios.post(`/api/info/agregar`,envioinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/envioslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function enviosRegresar()
    {
        window.location.href="/envioslistar";
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Envio</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="id" className="form-label">id</label>
                        <input type="text" className="form-control" id="id" value={id} onChange={(e) => {setId(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="direccion" className="form-label">Dirección</label>
                        <input type="text" className="form-control" id="direccion" value={direccion} onChange={(e) => {setDireccion(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="pais" className="form-label">Pais</label>
                        <input type="text" className="form-control" id="pais" value={pais} onChange={(e) => {setPais(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="departamento" className="form-label">Departamento</label>
                        <input type="text" className="form-control" id="departamento" value={departamento} onChange={(e) => {setDepartamento(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="ciudad" className="form-label">Ciudad</label>
                        <input type="text" className="form-control" id="ciudad" value={ciudad} onChange={(e) => {setCiudad(e.target.value)}}></input>
                    </div>         
                    <div className="mb-12">
                        <button type="button" onClick={enviosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={enviosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default EnviosAgregar;