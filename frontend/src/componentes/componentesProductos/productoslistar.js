import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import ProductosBorrar from './productoseliminar';


function ProductosListar()
{
    const[dataProductos, setdataProductos] = useState([])

    useEffect(()=>{
        axios.get('api/producto/listar').then(res => {
        console.log(res.data)
        setdataProductos(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])

return (
    <div className="container mt-5">
    <h4>Lista de Productos</h4>
    <div className="row">
        <div className="col-md-12">
            <table className="table table-dark">
                <thead>
                    <tr key={0}>
                        <td colSpan={6} align="right"><Link to={`/productosagregar`}><li className='btn btn-success'>Agregar Producto</li></Link></td>
                    </tr>
                    <tr key={0}>
                        <td align="center">Id</td>
                        <td>Id Categoria</td>
                        <td>Nombre</td>
                        <td>Descripción</td>
                        <td>Precio</td>
                        <td>Activo</td>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>
                </thead> 
                <tbody className='dialog'>
                {
                    dataProductos.map(miproducto => (
                    <tr key={miproducto.id}>
                        <td align="center">{miproducto.id}</td>
                        <td>{miproducto.id_categoria}</td>
                        <td>{miproducto.nombre}</td>
                        <td>{miproducto.descripcion}</td>
                        <td>{miproducto.precio}</td>
                        <td align="right">{miproducto.activo ? 'Activo' : 'Inactivo'}</td>
                        <td align="center"><Link to={`/productoseditar/${miproducto.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                        <td align="center"><li className='btn btn-danger' onClick={()=>{ProductosBorrar(miproducto.id)}}>Borrar</li></td>
                    </tr>
                ))
                }
                </tbody>
            </table>
        </div>
    </div>
    </div>
)

}


export default ProductosListar;